library(tm)
library(wordcloud)
library(memoise)
library(tidyr)
library(dplyr)
library(ggplot2)
library(RWeka)
library(wordcloud2)
library(network)
library(tibble)
library(igraph)
library(stringr)
library(reshape)
library(RMySQL)
library(usmap)
library(neuralnet)
library(tidytext)
library(factoextra)
library(e1071)
library(caTools)
library(caret)

setwd("C:/Users/jimsc/OneDrive/Documents/PHD/ForestResearch")

tweets <- read.csv("combined_forest2021-12-31.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets)

#View(tweets)
#remove the politically incorrect contributor "HersheSquirt"
tweets <- tweets %>% filter(!grepl("HersheSquirt", retweet_screen_name, ignore.case = TRUE))
tweets <- tweets %>% filter(!grepl("HersheSquirt", screen_name, ignore.case = TRUE))
tweets <- tweets %>% filter(!grepl("HersheSquirt", mentions_screen_name, ignore.case = TRUE))
nrow(tweets)


#############################################  Time series these tweets

#tweets$created_at <- as.Date(tweets$created_at)
ggplot(tweets,aes(x=created_at))+
  geom_bar(aes(y = (..count..))) + 
  coord_cartesian(ylim = c(0,500))


#############################################  Find the most favorited tweets

mostFavorited <- tweets[order(tweets$favorite_count, decreasing = TRUE),]
mostFavorited <- mostFavorited %>% filter(is_favorite == FALSE)
head(mostFavorited)
#11/15/2021 Girlfriend found a spotted lanternfly on the patio and promptly reported it to the New Jersey department of agriculture


#############################################  Find the most retweeted tweets

mostRetweeted <- tweets[order(tweets$retweet_count, decreasing = TRUE),]
mostRetweeted <- mostRetweeted %>% filter(is_retweet == FALSE)
head(mostRetweeted)
#11/15/2021 spotted lanternfly girl https://t.co/NjPkgRoG2M

#CBS News account, see below



#############################################  Run the Cluster analysis and the Wordcloud

corpus <- Corpus(VectorSource(tweets$text))
stripJoiners <- content_transformer(function(x, pattern) {return (gsub(pattern, " ", x))})
corpus <- tm_map(corpus, content_transformer(tolower))
corpus <- tm_map(corpus, removePunctuation)
corpus <- tm_map(corpus, removeNumbers)
corpus <- tm_map(corpus, stripWhitespace)
corpus <- tm_map(corpus, removeWords,
                 c(stopwords("en")))

ndocs <- length(corpus)
minTermFreq <- ndocs * 0.01
maxTermFreq <- ndocs * .9

dtm1 = DocumentTermMatrix(corpus,
                          control = list(
                            stopwords = TRUE,
                            #weighting = weightTfIdf,
                            wordLengths=c(3, 45),
                            removePunctuation = T,
                            removeNumbers = T,
                            stemming = F,
                            bounds = list(global = c(minTermFreq, maxTermFreq))
                          ))

#options(java.parameters = "-Xmx5g" )
m  <- as.matrix(dtm1)

rowFreq = rowSums(as.matrix(dtm1))
colFreq = colSums(as.matrix(dtm1))

tail(sort(rowFreq), n = 10)
tail(sort(colFreq), n = 10)
# # # m <- m[1:2, 1:3]
distMatrix <- dist(m, method="euclidean") #euclidean wasn't bad so maybe move it back, in fact, cosine gave error

#Find the right number of clusters
fviz_nbclust(m, kmeans, method = "silhouette", k.max = 20) #silhouette, wss, gap_stat
fviz_nbclust(m, kmeans, method = "wss", k.max = 20)

groups <- hclust(distMatrix,method="ward.D") #ward.D, complete, average was pretty good so perhaps move it back
fviz_dend(groups,
          k=4,
          labels_track_height = 3000,
          horiz = FALSE,
          type = "circular",#"phylogenic", #"circular",  #phylogenic
          color_labels_by_k = TRUE,
          k_colors = c("red", "green3", "blue", "magenta","gray", "purple","black", "brown","orange"))


trends <- cutree(groups, 4)
trends <- as.data.frame(trends)

head(trends)
clst <- tweets
head(clst)

nrow(trends)
nrow(clst)

#table(trends)
#clst <- clst %>% mutate(Joint = trends)

clst <- cbind(rownames(clst),clst)
names(clst)[1] <- 'Inquiry'
head(clst, 1)


trends <- cbind(rownames(trends),trends)
rownames(trends) <- NULL
colnames(trends) <- c("Inquiry","Group")
head(trends, 1)
nrow(trends)

#join up the trends and the goals datasets using the 'question.asked' field or whatever lives in the groups outcome of the tree...
#I don't think this works anymore, going to try the mutate option, interesting.https://uc-r.github.io/hc_clustering
clst <- left_join(clst, trends, by='Inquiry')


nrow(trend1 <- clst  %>% filter(Group == 1))
nrow(trend2 <- clst  %>% filter(Group == 2))
nrow(trend3 <- clst  %>% filter(Group == 3))
nrow(trend4 <- clst  %>% filter(Group == 4))

WCGroup=function(grp)
{
  corpus1 <- Corpus(VectorSource(grp$text))
  #print(grp$QUESTION.ASKED)
  stripJoiners <- content_transformer(function(x, pattern) {return (gsub(pattern, " ", x))})
  corpus1 <- tm_map(corpus1, stripJoiners, "-")
  corpus1 <- tm_map(corpus1, stripJoiners, "\200\231")
  corpus1 <- tm_map(corpus1, content_transformer(tolower))
  corpus1 <- tm_map(corpus1, removePunctuation)
  corpus1 <- tm_map(corpus1, removeNumbers)
  corpus1 <- tm_map(corpus1, stripWhitespace)
  corpus1 <- tm_map(corpus1, removeWords,
                    c(stopwords("en"), "ai","iot"))
  
  token_delim <- " \\t\\r\\n. !?,;\"()"
  Token <- NGramTokenizer(corpus1, Weka_control(min=2,max=4,delimiters=token_delim))
  Words <- data.frame(table(Token))
  Sort <- Words[order(Words$Freq, decreasing = TRUE),]
  print(head(Sort,10))
  #ggplot(Tops, aes())
  wordcloud(Sort$Token,Sort$Freq, scale = c(3.7,.2),min.freq = 50, max.words = 30, colors=brewer.pal(6, "Dark2"), res = 3000)
}

WCGroup(trend1)
WCGroup(trend2)
WCGroup(trend3)
WCGroup(trend4)


#############################################  PLOT THE US MAP AND LOCATIONS
#
#
#
#

head(tweets$location)

tweetsLocation <- tweets %>% select(created_at, text, screen_name,
                                    quoted_location, place_url, place_name,
                                    place_full_name, geo_coords, location,
                                    bbox_coords, is_retweet)
#tweetsLocation <- tweetsLocation %>% filter(!grepl("NA NA NA NA NA NA NA NA", bbox_coords, ignore.case = TRUE))
tweetsLocation <- tweetsLocation %>% filter(is_retweet == "FALSE")
#tweetsLocation$location
#tweetsLocation$bbox_coords

locations <- ""
#stuff  <- str_split_fixed(tweetsLocation$location, ",", 2)[,2]
locations$state <- unlist(strsplit(tweetsLocation$location,","))
locations$state <- trimws(locations$state,which="both")
locations <- as.data.frame(locations)
locations$defaultNum = 1
nrow(locations)
locations <- locations %>%
  group_by(locations$state) %>%
  mutate(value = sum(defaultNum)) %>%
  select(state, value)

max(locations$value)

statesCenters <- as.data.frame(state.center)
statesCenters$name <- state.name
statesCenters <- inner_join(statesCenters, statepop, by=c("name"="full"))

#locations <- locations %>% filter(nchar(as.character(state))==2)
locations$abbr <- locations$state
locations <- inner_join(locations, statepop, by = "abbr")
tail(locations,15)



locations <- as.data.frame(locations)

zeroTweets <- data.frame('locations$state'  = c("LA","AR","SD","ND","MT"),
                         state = c("LA","AR","SD","ND","MT"),
                         value = c(0,0,0,0,0),
                         abbr = c("LA","AR","SD","ND","MT"),
                         fips = c("22","05","46","38","30"),
                         full = c("Louisiana","Arkansas","South Dakota","North Dakota", "Montana"),
                         pop_2015 = c(0,0,0,0,0)
)

colnames(zeroTweets)[which(names(zeroTweets) == "locations.state")] <- "locations$state"

locations <- rbind(zeroTweets,locations)

View(statepop)

cities <- usmap_transform(citypop)

#aphis <- Connecticut, Delaware, Indiana, Maryland, Massachusetts, New Jersey, 
#New York, Ohio, Pennsylvania, Virgina, and West Virginia 
aphis <- data.frame(locations = c("CT", "DE", "IN", "MD", "MA", "NJ","NY", "OH", "PA", "VA", "WV"),
                    state = c("CT", "DE", "IN", "MD", "MA", "NJ","NY", "OH", "PA", "VA", "WV"),
                    value = c(1,1,1,1,1,1,1,1,1,1,1),
                    abbr = c("CT", "DE", "IN", "MD", "MA", "NJ","NY", "OH", "PA", "VA", "WV"),
                    fips = c("09","10","18","24","25", "34","36","39","42","51","54")
)

aphis <- inner_join(aphis, cities, by = "abbr")

predicted <- data.frame(locations = c("MO","MN"),
                           state = c("MO","MN"),
                           value = c(1,1),
                           abbr = c("MO","MN"),
                           fips = c("29","27")
)
cities <- cities %>% add_row(lon = -92.603760, lat = 38.52, 
                             state = "Missouri", 
                             abbr = "MO", 
                             most_populous_city = "Jefferson City",
                             city_pop = 10000000000,
                             lon.1 = 571878.58,
                             lat.1 = -692694.69)
cities <- cities %>% filter(!grepl("Kansas City", most_populous_city, ignore.case = TRUE))

predicted <- inner_join(predicted, cities, by = "abbr")

glimpse(aphis)
glimpse(predicted)
  
plot_usmap(data = locations, values = "value", color = "red") +
  scale_fill_continuous(
    low = "white", high = "red", name = "SLF Tweets per State", label = scales::comma,
                        limits = c(0,400)) + 
  geom_point(data = aphis, 
             aes(x = lon.1, y = lat.1),  shape = 18, color = "black", size = 3) +
  geom_point(data = predicted, 
             aes(x = lon.1, y = lat.1),  shape = 17, color = "black", size = 3) +
    labs(title = "Digital Twin of the SLF Propagation", subtitle = "Predicting New Sightings") +
  theme(legend.position = "right") 


kills <- tweets %>% filter(is_retweet == FALSE) %>% filter(grepl("squash|kill|dead|trap|get rid|smash",text,ignore.case = TRUE))
nrow(kills)
head(kills)
tail(kills)

kills <- kills %>% mutate(kill = if_else(grepl("squash|kill|dead|trap|get rid|smash",text,ignore.case = TRUE), 1, 0))

killCount <- kills %>% filter(grepl("squash|kill|dead|trap|get rid|smash",text,ignore.case = TRUE)) %>% tally()

###############################################################################
#
#   Look into the hashtags overtime


hashtagsLong <- tweets %>%
  unnest_tokens(word, hashtags) %>% select(word, created_at) %>% na.omit() %>% unique()
hashtagsLong$count = 1
hashtagsWide <- spread(hashtagsLong, key = word, value = count)
hashtagsWide <- hashtagsWide %>% select(-created_at) %>% as.integer()
head(hashtagsWide)

hashtagsWide <- as.data.frame(hashtagsWide)

hashtagsSum <- hashtagsWide %>%
  summarize_if(is.numeric, sum, na.rm=TRUE)

topHashes <- sort(colSums(hashtagsSum[,1:length(hashtagsSum)]),decreasing=TRUE)[1:10]

genmilley <- tweets %>% filter(grepl("genmilley", hashtags, ignore.case = TRUE))
china <- tweets %>% filter(grepl("china", hashtags, ignore.case = TRUE))
yunnan <- tweets %>% filter(grepl("yunnan", hashtags, ignore.case = TRUE))

tweets <- tweets %>% mutate(Lanternfly = if_else(grepl("#lanternfly", hashtags, ignore.case = TRUE), 1, 0))
tweets <- tweets %>% mutate(SpottedLanternfly = if_else(grepl("spottedlanternfly", hashtags, ignore.case = TRUE), 1, 0))
tweets <- tweets %>% mutate(Badbug = if_else(grepl("badbug", hashtags, ignore.case = TRUE), 1, 0))
tweets <- tweets %>% mutate(InvasiveSpecies = if_else(grepl("invasivespecies", hashtags, ignore.case = TRUE), 1, 0))
tweets <- tweets %>% mutate(Insect = if_else(grepl("insect", hashtags, ignore.case = TRUE), 1, 0))
tweets <- tweets %>% mutate(China = if_else(grepl("china", hashtags, ignore.case = TRUE), 1, 0))
tweets <- tweets %>% mutate(NYC = if_else(grepl("nyc", hashtags, ignore.case = TRUE), 1, 0))
tweets <- tweets %>% mutate(Yunnan = if_else(grepl("yunnan", hashtags, ignore.case = TRUE), 1, 0))
tweets <- tweets %>% mutate(Bugs = if_else(grepl("bugs", hashtags, ignore.case = TRUE), 1, 0))
tweets <- tweets %>% mutate(Agriculture = if_else(grepl("agriculture", hashtags, ignore.case = TRUE), 1, 0))

abis <- strptime(hashtags$created,format="%d/%m/%Y") #defining what is the original format of your date
hashtags$created_at <- as.Date(abis,format="%Y-%m-%d") #defining what is the desired format of your date

tweets$created_at <- as.Date(tweets$created_at)
ByDate <- tweets %>% select(created_at, Lanternfly, SpottedLanternfly, Badbug, InvasiveSpecies, Insect, China, NYC, Yunnan, Bugs, Agriculture) %>%
  gather(key = "UseCase", value = "Mentions", -created_at)
#View(ByDate)

ByDate <- as.data.frame(ByDate %>% group_by(created_at, UseCase) %>% summarise(Daily = sum(Mentions)))

####Strip anything 2019 off
# ByDate <- ByDate %>% filter(created_at >= "2020-01-01")
# ByDate <- ByDate %>% filter(created_at <= "2021-01-01")
ByDate$UseCase <- as.factor(ByDate$UseCase)

ggplot(ByDate, aes(x = created_at, y = Daily,  color = UseCase)) +
  geom_point(size = 1) +
  geom_smooth(method="loess", color="purple") +
  geom_smooth(method="lm", color="green") +
  facet_wrap( ~ UseCase, nrow = 2) +
  #geom_line(aes(color=UseCase), size = 1) +
  scale_x_date(date_labels = "%b/%Y") +
  xlab("Tweets Created Date") + ylab("Daily Count of Top 10 Hashtags)") +
  #scale_y_log10() +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 45, vjust = .5)) +
  theme(strip.text.x = element_text(size = 19)) +
  coord_cartesian(ylim = c(0,5))

########################################        Plot Locations
#
#
#
plotLocations <- tweets
plotLocations <- plotLocations %>%
  count(location, sort = TRUE) %>%
  mutate(location = reorder(location,n)) %>%
  na.omit() %>%
  filter(grepl(",",location, ignore.case = TRUE)) %>% top_n(10) %>%
  ggplot(aes(x = location,y = n)) +
  geom_col() +
  coord_flip() +
  labs(x = "Location",
       y = "Count",
       title = "Spotted Lanterfly tweets Locations")
plotLocations


#############################################   Wordcloud the Hashtags
#
#
#

wcFlys <- tweets %>% select(hashtags)
wcFlys$hashtags <- as.character(wcFlys$hashtags )
wcFlys$hashtags <- gsub("c\\(", "", wcFlys$hashtags)set.seed(1234)
wordcloud(wcFlys$hashtags, min.freq=5, scale=c(3.5, .5), random.order=FALSE, rot.per=0.35,
          colors=brewer.pal(8, "Dark2"))



################################################################################
#
#                 ML for factors/regression/prediction
corpus <- tweets #%>% filter(is_retweet == FALSE)
corpus <- corpus %>% mutate(kill = if_else(grepl("squash|kill|dead|trap|get rid|smash",text,ignore.case = TRUE), 1, 0))
corpus <- corpus %>% mutate(states = str_split_fixed(corpus$location, ",", 2)[,2])
corpus$text <- as.character(corpus$text)

corpus <- corpus %>%
  unnest_tokens(word, text) %>%
  anti_join(stop_words, by = "word") %>%
  mutate(word_lemma = textstem::lemmatize_words(word)) %>%
  select(-word)

head(corpus)

#I have to cite this lexacon if I use it in paper

corpus <- corpus %>%
  left_join(get_sentiments("nrc"), by = c("word_lemma" = "word"))

corpus <- corpus %>% select(favorite_count, kill, sentiment, location, retweet_count, states, created_at)

nrow(corpus)
corpus <- na.omit(corpus)
corpus <- corpus %>% mutate(favorite = ifelse(favorite_count  > 0, 1, 0))
corpus <- corpus %>% mutate(retweet = ifelse(retweet_count > 0, 1, 0))
nrow(corpus)

#split data frame into training and testing sets
#double check
set.seed(123)   #  set seed to ensure you always have same random numbers generated
sample = sample.split(corpus,SplitRatio = 0.60) # splits the data in the ratio mentioned in SplitRatio. After splitting marks these rows as logical TRUE and the the remaining are marked as logical FALSE
train = subset(corpus, sample ==TRUE) # creates a training dataset named train with rows which are marked as TRUE
test = subset(corpus, sample==FALSE)

train$kill <- as.factor(train$kill)
test$kill <- as.factor(test$kill)
train$sentiment <- as.factor(train$sentiment)
test$sentiment <- as.factor(test$sentiment)
train$favorite_count <- as.integer(train$favorite_count)
test$retweet_count <- as.integer(test$retweet_count)
train$favorite <- as.factor(train$favorite)
test$favorite <- as.factor(test$favorite)
train$retweet <- as.factor(train$retweet)
test$retweet <- as.factor(test$retweet)
train$location <- as.factor(train$location)
test$location <- as.factor(test$location)

head(train)

table(test$kill) %>% prop.table()
table(train$kill) %>% prop.table()
# 
# #64% accurate whereas the nb approaches 77% accurate
# logit <- glm(kill~retweet, family = "binomial", data=train)
# predict <- predict(logit, data=test, type = 'response')
# confusionMatrix <- table(train$kill, predict)
# confusionMatrix
# 
# nrow(test)
# nrow(predict)

#92.28 accuracy
NBclassfier=naiveBayes(kill~sentiment+favorite+location+retweet, data=train)
NBLabelBySentLemma=function(model)
{
  #don't touch, this model gets X% accuracy
  trainPred=predict(model, newdata = train, type = "class")
  trainTable=table(train$kill, trainPred)
  testPred=predict(model, newdata=test, type="class")
  testTable=table(test$kill, testPred)
  #add or subtract the matrix to include counting more variables, such as [3,3]
  trainAcc=(trainTable[1,1]+trainTable[2,2])/sum(trainTable)
  testAcc=(testTable[1,1]+testTable[2,2])/sum(testTable)
  message("Contingency Table for Training Data")
  print(trainTable)
  message("Contingency Table for Test Data")
  print(testTable)
  message("Accuracy")
  print(round(cbind(trainAccuracy=trainAcc, testAccuracy=testAcc),3))
  NBclassfier
  confusionMatrix(testPred,test$kill)
}
NBLabelBySentLemma(NBclassfier)

#67.04 accuracy
NBclassfier=naiveBayes(kill~sentiment+favorite+retweet, data=train)
NBLabelBySentLemma=function(model)
{
  #don't touch, this model gets X% accuracy
  trainPred=predict(model, newdata = train, type = "class")
  trainTable=table(train$kill, trainPred)
  testPred=predict(model, newdata=test, type="class")
  testTable=table(test$kill, testPred)
  #add or subtract the matrix to include counting more variables, such as [3,3]
  trainAcc=(trainTable[1,1]+trainTable[2,2])/sum(trainTable)
  testAcc=(testTable[1,1]+testTable[2,2])/sum(testTable)
  message("Contingency Table for Training Data")
  print(trainTable)
  message("Contingency Table for Test Data")
  print(testTable)
  message("Accuracy")
  print(round(cbind(trainAccuracy=trainAcc, testAccuracy=testAcc),3))
  NBclassfier
  confusionMatrix(testPred,test$kill)
}
NBLabelBySentLemma(NBclassfier)

#69.49 accuracy
NBclassfier=naiveBayes(kill~sentiment+retweet+states, data=train)
NBLabelBySentLemma=function(model)
{
  #don't touch, this model gets X% accuracy
  trainPred=predict(model, newdata = train, type = "class")
  trainTable=table(train$kill, trainPred)
  testPred=predict(model, newdata=test, type="class")
  testTable=table(test$kill, testPred)
  #add or subtract the matrix to include counting more variables, such as [3,3]
  trainAcc=(trainTable[1,1]+trainTable[2,2])/sum(trainTable)
  testAcc=(testTable[1,1]+testTable[2,2])/sum(testTable)
  message("Contingency Table for Training Data")
  print(trainTable)
  message("Contingency Table for Test Data")
  print(testTable)
  message("Accuracy")
  print(round(cbind(trainAccuracy=trainAcc, testAccuracy=testAcc),3))
  NBclassfier
  confusionMatrix(testPred,test$kill)
}
NBLabelBySentLemma(NBclassfier)

#70.62 accuracy
NBclassfier=naiveBayes(kill~sentiment+states, data=train)
NBLabelBySentLemma=function(model)
{
  #don't touch, this model gets X% accuracy
  trainPred=predict(model, newdata = train, type = "class")
  trainTable=table(train$kill, trainPred)
  testPred=predict(model, newdata=test, type="class")
  testTable=table(test$kill, testPred)
  #add or subtract the matrix to include counting more variables, such as [3,3]
  trainAcc=(trainTable[1,1]+trainTable[2,2])/sum(trainTable)
  testAcc=(testTable[1,1]+testTable[2,2])/sum(testTable)
  message("Contingency Table for Training Data")
  print(trainTable)
  message("Contingency Table for Test Data")
  print(testTable)
  message("Accuracy")
  print(round(cbind(trainAccuracy=trainAcc, testAccuracy=testAcc),3))
  NBclassfier
  confusionMatrix(testPred,test$kill)
}
NBLabelBySentLemma(NBclassfier)

#86.44 accuracy
NBclassfier=naiveBayes(kill~location, data=train)
NBLabelBySentLemma=function(model)
{
  #don't touch, this model gets X% accuracy
  trainPred=predict(NBclassfier, newdata = train, type = "class")
  trainTable=table(train$kill, trainPred)
  testPred=predict(NBclassfier, newdata=test, type="class")
  testTable=table(test$kill, testPred)
  #add or subtract the matrix to include counting more variables, such as [3,3]
  trainAcc=(trainTable[1,1]+trainTable[2,2])/sum(trainTable)
  testAcc=(testTable[1,1]+testTable[2,2])/sum(testTable)
  message("Contingency Table for Training Data")
  print(trainTable)
  message("Contingency Table for Test Data")
  print(testTable)
  message("Accuracy")
  print(round(cbind(trainAccuracy=trainAcc, testAccuracy=testAcc),3))
  NBclassfier
  confusionMatrix(testPred,test$kill)
}
NBLabelBySentLemma(NBclassfier)

train$kill  <- as.numeric(train$kill)
train$retweet <- as.numeric(train$retweet)
train$favorite <- as.numeric(train$favorite)

test$kill  <- as.numeric(test$kill)
test$retweet <- as.numeric(test$retweet)
test$favorite <- as.numeric(test$favorite)

nn = neuralnet(kill~favorite+retweet, data = train, hidden=3, act.fct="logistic", linear.output = FALSE)
plot(nn)

Predict = compute(nn,test)
prob <- Predict$net.result
pred <- ifelse(prob>0.3,1,0)
pred
summary(pred)
tots <- as.data.frame(pred) %>% filter(prob > 0) %>% tally()

nrow(pred)
rlang::last_error()


##############################################################################
# Test the theory on weak links by checking retweeters versus OP's friends
#
##############################################################################



rets <- tweets %>% filter(grepl("There's a beautiful spotted insect flying across the U.S.*and officials want you to kill it",
                                text, ignore.case = TRUE))
retsScreenNames <- rets$screen_name
retsScreenNames <- as.data.frame(retsScreenNames)
#CBSOnlyDF <- data.frame("CBSNews")
#retsScreenNames <- merge(CBSOnlyDF, retsScreenNames)
retsScreenNames <- subset(retsScreenNames,!duplicated(retsScreenNames))
retsScreenNames$ID <- seq.int(nrow(retsScreenNames))
head(retsScreenNames)
nrow(retsScreenNames)

CBSNewsFriends <- get_friends("CBSNews")
CBSNewsFriendsData <- lookup_users(CBSNewsFriends$user_id)
CBSFriendsScreenNames <- CBSNewsFriendsData$screen_name
CBSFriendsScreenNames <- as.data.frame(CBSFriendsScreenNames)
CBSFriendsScreenNames$User <- "CBSNews"
#CBSFriendsScreenNames$ID <- seq.int(nrow(CBSFriendsScreenNames))
head(retsScreenNames)
head(CBSFriendsScreenNames)
nrow(retsScreenNames)

#join both sets of screennames, create NAs for when "CBS" doesn't show up, 
#that will tell us how many orphaned retweeters that aren't friends
Nodes <- left_join(retsScreenNames, CBSFriendsScreenNames, 
                          by = c("retsScreenNames" = "CBSFriendsScreenNames"))
nrow(Nodes)
tail(Nodes)
# asdf <- Nodes %>% filter(grepl("cbs", User, ignore.case = TRUE))
# checkforCBS <- Nodes %>% filter(grepl("cbs", retsScreenNames, ignore.case = TRUE))
#Nodes <- na.omit(Nodes)

Edges <- Nodes %>% na.omit(Nodes)
names(Edges)[names(Edges) == "ID"] <- "To"
Edges <- Edges %>% left_join(Nodes, by=c("User"="retsScreenNames"))
names(Edges)[names(Edges) == "ID"] <- "From"
names(Edges)[names(Edges) == "ID"] <- "From"

Edges <- Edges %>% select("From","To")
c(Edges[,1], Edges[,2]) %in% Nodes$ID

#reorder Nodes to make ID first column
Nodes <- Nodes %>% select("ID","retsScreenNames")

routes_network <- network(Edges, vertex.attr = Nodes, matrix.type = "edgelist", ignore.eval = TRUE)

class(routes_network)

summary(routes_network)
plot(routes_network, vertex.cex = 3)

routes_igraph <- graph_from_data_frame(d = Edges, vertices = Nodes, directed = TRUE)

plot(routes_igraph, layout = layout_nicely, edge.arrow.size = 2.2)
plot(routes_igraph, layout = layout_in_circle, edge.arrow.size = 0.6, margin= .1) #layout_nicely, layout_on_sphere, layout_with_fr
plot(routes_igraph, 
     layout = layout_as_star(routes_igraph,center = V(routes_igraph)[46]),
     edge.arrow.size = .2, 
     vertex.label = Nodes$retsScreenNames,
     edge.label=Edges$retsScreenNames, 
     vertex.size=1, edge.color="darkgreen", 
     vertex.label.font=1, edge.label.font =1, edge.label.cex = 1, 
     vertex.label.cex = .5)


###################################################################################################
#SLF activity increases in mid-september, do we see that here?
#################################  Do the tweets become more in early to mid-September?

tweets0 <- read.csv("combined_forest2021-09-15.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets1)
tweets1 <- read.csv("combined_forest2021-09-20a.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets1)
tweets2 <- read.csv("combined_forest2021-09-25.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets2)
tweets3 <- read.csv("combined_forest2021-10-01.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets3)
tweets4 <- read.csv("combined_forest2021-10-10.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets4)
tweets5 <- read.csv("combined_forest2021-10-14.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets5)
tweets6 <- read.csv("combined_forest2021-10-22.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets6)
tweets7 <- read.csv("combined_forest2021-10-23.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets7)
tweets7a <- read.csv("combined_forest2021-10-25.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets7a)
tweets8 <- read.csv("combined_forest2021-10-31.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets8)
tweets9 <- read.csv("combined_forest2021-11-05.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets9)
tweets10 <- read.csv("combined_forest2021-11-09.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets10)
tweets11 <- read.csv("combined_forest2021-11-11.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets11)
tweets12 <- read.csv("combined_forest2021-11-15.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets12)
tweets13 <- read.csv("combined_forest2021-11-20.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets13)
tweets14 <- read.csv("combined_forest2021-11-23.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets14)
tweets15 <- read.csv("combined_forest2021-11-27.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets15)
tweets16 <- read.csv("combined_forest2021-12-02.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets16)
tweets17 <- read.csv("combined_forest2021-12-08.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets17)
tweets18 <- read.csv("combined_forest2021-12-11.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets18)
tweets19 <- read.csv("combined_forest2021-12-17.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE, skip = 0, stringsAsFactors = FALSE)
nrow(tweets19)

tweets5$status_id <- as.character(tweets5$status_id)
tweets6$status_id <- as.character(tweets6$status_id)

t19 <- subset(tweets19, !(status_id %in% tweets18$status_id))
t18 <- subset(tweets18, !(status_id %in% tweets17$status_id))
t17 <- subset(tweets17, !(status_id %in% tweets16$status_id))
t16 <- subset(tweets16, !(status_id %in% tweets15$status_id))
t15 <- subset(tweets15, !(status_id %in% tweets14$status_id))
t14 <- subset(tweets14, !(status_id %in% tweets13$status_id))
t13 <- subset(tweets13, !(status_id %in% tweets12$status_id))
t12 <- subset(tweets12, !(status_id %in% tweets11$status_id))
t11 <- subset(tweets11, !(status_id %in% tweets10$status_id))
t10 <- subset(tweets10, !(status_id %in% tweets9$status_id))
t9 <- subset(tweets9, !(status_id %in% tweets8$status_id))
t8 <- subset(tweets8, !(status_id %in% tweets7a$status_id))
t7a <- subset(tweets7a, !(status_id %in% tweets7$status_id))
t7 <- subset(tweets7, !(status_id %in% tweets6$status_id))
t6 <- subset(tweets6, !(text %in% tweets5$text))
t5 <- subset(tweets5, !(status_id %in% tweets4$status_id))
t4 <- subset(tweets4, !(status_id %in% tweets3$status_id))
t3 <- subset(tweets3, !(status_id %in% tweets2$status_id))
t2 <- subset(tweets2, !(status_id %in% tweets1$status_id))
t1a <- subset(tweets1, !(status_id %in% tweets0$status_id))
t0 <- tweets0
#oldest <- subset(tweets3, !(status_id %in% tweets2$status_id))

t0 <- t0 %>% mutate(plotDate = "2021-09-15") %>% select(plotDate,text)
t1 <- t1 %>% mutate(plotDate = "2021-09-20") %>% select(plotDate,text)
t2 <- t2 %>% mutate(plotDate = "2021-09-25") %>% select(plotDate,text)
t3 <- t3 %>% mutate(plotDate = "2021-10-01") %>% select(plotDate,text)
t4 <- t4 %>% mutate(plotDate = "2021-10-10") %>% select(plotDate,text)
t5 <- t5 %>% mutate(plotDate = "2021-10-14") %>% select(plotDate,text)
t6 <- t6 %>% mutate(plotDate = "2021-10-22") %>% select(plotDate,text)
t7 <- t7 %>% mutate(plotDate = "2021-10-23") %>% select(plotDate,text)
t7a <- t7a %>% mutate(plotDate = "2021-10-25") %>% select(plotDate,text)
t8 <- t8 %>% mutate(plotDate = "2021-10-31") %>% select(plotDate,text)
t9 <- t9 %>% mutate(plotDate = "2021-11-05") %>% select(plotDate,text)
t10 <- t10 %>% mutate(plotDate = "2021-11-09") %>% select(plotDate,text)
t11 <- t11 %>% mutate(plotDate = "2021-11-11") %>% select(plotDate,text)
t12 <- t12 %>% mutate(plotDate = "2021-11-15") %>% select(plotDate,text)
t13 <- t13 %>% mutate(plotDate = "2021-11-20") %>% select(plotDate,text)
t14 <- t14 %>% mutate(plotDate = "2021-11-23") %>% select(plotDate,text)
t15 <- t15 %>% mutate(plotDate = "2021-11-27") %>% select(plotDate,text)
t16 <- t16 %>% mutate(plotDate = "2021-12-02") %>% select(plotDate,text)
t17 <- t17 %>% mutate(plotDate = "2021-12-08") %>% select(plotDate,text)
t18 <- t18 %>% mutate(plotDate = "2021-12-11") %>% select(plotDate,text)
t19 <- t19 %>% mutate(plotDate = "2021-12-17") %>% select(plotDate,text)

nrow(t6)
nrow(t7)
nrow(t7a)
nrow(t8)
nrow(t9)
nrow(t10)

Lifecycle <- NA
Lifecycle <- rbind(t0,t1,t2,t3,t4,t5,t6,t7,t7a,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19)


Lifecycle$plotDate <- as.Date(Lifecycle$plotDate) #, "%m/%d/%Y")
tail(Lifecycle)
forSentTrends <- Lifecycle

Lifecycle <- as.data.frame(Lifecycle %>% group_by(plotDate)  %>% dplyr::summarise(Daily = n()))
nrow(Lifecycle)

####Strip anything 2019 off
# ByDate <- ByDate %>% filter(created_at >= "2020-01-01")
# ByDate <- ByDate %>% filter(created_at <= "2021-01-01")

ggplot(Lifecycle, aes(x = plotDate, y = Daily)) +
  geom_point(size = 1) +
  geom_smooth(method="loess", color="purple") +
  geom_smooth(method="lm", color="green") +
  #scale_x_date(date_labels = "%b/%Y") +
  xlab("Tweets Created Date") + ylab("Tweets Posted") +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 45, vjust = .5)) +
  theme(strip.text.x = element_text(size = 19)) +
  coord_cartesian(ylim = c(0,10000))



################################################################################
#   Plot sentiment over time series for social unrest study
#
###############################################################################

corpus <- forSentTrends 
corpus$text <- as.character(corpus$text)

corpus <- corpus %>%
  unnest_tokens(word, text) %>%
  anti_join(stop_words, by = "word") %>%
  mutate(word_lemma = textstem::lemmatize_words(word)) %>%
  select(-word)

head(corpus)

#I have to cite this lexacon if I use it in paper

corpus <- corpus %>%
  left_join(get_sentiments("nrc"), by = c("word_lemma" = "word"))

corpus <- corpus %>% select(sentiment, plotDate)

tsSentiment <- corpus %>% filter(!is.na(sentiment))
nrow(tsSentiment)
head(tsSentiment)
tsSentiment <- as.data.frame(tsSentiment)
tsSentiment <- tsSentiment %>% select(sentiment, plotDate) 
tsSentiment <- tsSentiment %>% group_by(sentiment,plotDate) %>% 
  dplyr::summarise(Daily = n()) 

ggplot(tsSentiment, aes(x = plotDate, y = Daily, color = sentiment)) +
  geom_point(size = 1) +
  geom_smooth(method="loess", color="purple") +
  geom_smooth(method="lm", color="green") +
  facet_wrap( ~ sentiment, nrow = 2) +
  #scale_x_date(date_labels = "%b/%Y") +
  xlab("Tweets Created Date") + ylab("Tweets Posted") +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 45, vjust = .5)) +
  theme(strip.text.x = element_text(size = 19)) +
  coord_cartesian(ylim = c(0,10000))


tsAverage <- tsSentiment %>% 
  group_by(plotDate) %>%  
  mutate(Average = average_mean(Daily),standardDeviation = sd(Daily)) %>% 
  ungroup()

tsSD <- tsSentiment %>% 
  group_by(sentiment,plotDate) %>%  
  dplyr::summarise(StdDev = StdDev(Daily))

tsSD <- tsSentiment %>% 
  group_by(plotDate) %>% 
  dplyr::mutate(standardDeviation = sd(Daily))

tsZScore <- tsAverage %>% 
  group_by(plotDate) %>%  
  mutate(ZScore = (Daily - Average)/standardDeviation ) %>% 
  ungroup()

maxZ = max(tsZScore$ZScore)
minZ = min(tsZScore$ZScore)

second_to_last_plotDate <- max(tsZScore$plotDate[tsZScore$plotDate != max(tsZScore$plotDate)])

ggplot(tsZScore, aes(x = plotDate, y = ZScore, color = sentiment)) +
  geom_point(size = 1) +
  geom_smooth(method="loess") +
  geom_label_repel(aes(x = second_to_last_plotDate, y = ZScore, 
                       label = sentiment, 
                       color = sentiment), 
                   nudge_x = 5) +
  xlab("Tweets Created Date") + ylab("Sentiment ZScore") +
  theme_bw()


  geom_label_repel(data = filter(tsZScore, plotDate == second_to_last_plotDate), 
                   aes(label = sentiment),
                   nudge_x = .75,
                   na.rm = TRUE) 
  
  
############################################
# Find and anger at goverment tweet from November during sentiment spike
#
###########################################
  
angerGov <- forSentTrends %>% 
  filter(plotDate >= "2021-11-10") %>% 
    filter(plotDate <= "2021-11-30")
  filter(grepl("govern", text, ignore.case = TRUE)) %>%
    filter(grepl("government", text, ignore.case = TRUE))
  
  

################################################################################
#       Experiment getting created_at dates back...
#
################################################################################

gcDate <- get_collections(status_id = "1436480000000000000")
View(gcDate)
