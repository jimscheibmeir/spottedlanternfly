library(rtweet)
library(ggplot2)
library(dplyr)
library(plyr)
library(tidyr)
library(tibble)
library(twitteR)
library(RCurl)
library(ggplot2)
library(reshape)
library(sentimentr)
library(RMySQL)
library(dplyr)
library("corpus")
library(wordcloud)
library(RWeka)
library(tm)
library(ggpubr)
library(RColorBrewer)
library(ggrepel) 
library(dataMeta)
library(textclean)
library(VennDiagram)
library(formattable)
library(maps)
head(us.cities)
# 
#install.packages("sos")
#library(sos)
#findFn("sample.split")

setwd("C:/Users/jimsc/OneDrive/Documents/PHD/ForestResearch")


c_key <- ''
c_secret<- ''
access_token <- ''
access_secret <- ''
appdt <- ''

twitter_token <- create_token(
  app = appdt,
  consumer_key = c_key,
  consumer_secret = c_secret,
  access_token = access_token,
  access_secret = access_secret)

setup_twitter_oauth(consumer_key, consumer_secret, access_token, access_secret)

st=format(Sys.time(), "%Y-%m-%d")
print(st)


forest_tweets27 <- search_tweets(q = "lanternfly",
                                 since = "2021-12-29", until = "2021-12-30", n = 50000, parse = TRUE, retryonratelimit = TRUE)
forest_tweets28 <- search_tweets(q = "lanternfly",
                                 since = "2021-12-30", until = "2021-12-31", n = 50000, parse = TRUE, retryonratelimit = TRUE)
forest_tweets29 <- search_tweets(q = "lanternfly",
                                 since = "2021-12-31", until = "2022-01-01", n = 50000, parse = TRUE, retryonratelimit = TRUE)


temp <- read.csv("combined_forest2021-12-28.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE)
temp <- select(temp, -"X")
temp1 <- read.csv("forest2021-09-20.csv", header=TRUE, sep=",",fill = TRUE, skipNul = TRUE)
temp1 <- select(temp1, -X)
ncol(temp)
ncol(temp1)
nrow(temp)


forest_tweets <- rbind(forest_tweets27,forest_tweets28,forest_tweets29)
nrow(forest_tweets)
ncol(forest_tweets)

flattened_ft <- flatten(forest_tweets)

temp1 <- rbind(temp, flattened_ft)
nrow(temp1)

tail(temp1)

write.csv(as.data.frame(temp1),paste('combined_forest', st, '.csv', sep=""))


